# Novation-Mininova-Ctrlr

Ctrlr Panels for the Novation Mininova

These are not all-singing-all-dancing panels, they meet a specific need I have to make my editing workflow a little easier and are offered up to you if they are useful.

**Mininova Tweak Aid**

Presets for the 8 tweak settings 

![OSC1](screenshots/tweak-osc1.jpg)

![Mixer](screenshots/tweak-mixer.jpg)

**Mininova Gator Aid**

The Mininova cannot edit gator patterns throuhgh the menu like the Ultranova can. This tool allows you to modify the patterns.

